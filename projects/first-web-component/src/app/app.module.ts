import {DoBootstrap, Injector, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { DiscountTagComponent } from './discount-tag/discount-tag.component';
import {createCustomElement} from "@angular/elements";

@NgModule({
  declarations: [
    DiscountTagComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [DiscountTagComponent]
})
export class AppModule implements DoBootstrap {

  constructor(private injector: Injector) {
    const webComponent = createCustomElement(DiscountTagComponent, {injector});
    customElements.define('discount-tag', webComponent);
  }

  ngDoBootstrap() {}
}
