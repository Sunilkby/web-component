import {Component, Input, OnInit} from '@angular/core';

@Component({
  templateUrl: './discount-tag.component.html',
  styleUrls: ['./discount-tag.component.scss']
})
export class DiscountTagComponent implements OnInit {
  @Input() discount: number = 10;

  constructor() { }

  ngOnInit(): void {
  }

}
